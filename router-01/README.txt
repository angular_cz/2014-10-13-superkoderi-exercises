Router-01

1. definujte routu pro OrderListController (as list)

2. zajistěte přesměrování na seznam, i při zadání jiné adresy

3. definujte routu pro OrderDetailController (as detail) 
 - v resolve načtěte data z http://angularczorders.apiary-mock.com/orders/ID jako orderData
 - všimněte si rozdílu chování

4. přidejte do detailu odkaz zpět na seznam