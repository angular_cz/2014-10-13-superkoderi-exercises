angular.module('orderAdministration', ['ngRoute'])
    .config(function($routeProvider) {

      $routeProvider
          .when('/orders', {
            templateUrl: 'orderList.html',
            controller: 'OrderListController',
            controllerAs: 'list'
          })
          .when('/detail/:id', {
            templateUrl: 'orderDetail.html',
            controller: 'OrderDetailController',
            controllerAs: 'detail',
            resolve: {
              orderData: function($http, $route) {
                return $http.get('http://angularczorders.apiary-mock.com/orders/' + $route.current.params.id)
                    .then(function(response) {
                      return response.data;
                    });
              }
            }
          })
          .otherwise('/orders');
    })
    .controller('OrderListController', function($http) {
      this.orders = [];

      this.statuses = {
        new : 'Nová',
        made: 'Vyrobená',
        cancelled: 'Zrušená',
        paid: 'Zaplacená',
        sent: 'Odeslaná'
      };

      this.onOrdersLoad = function(orders) {
        this.orders = orders;
      };

      $http.get('http://angularczorders.apiary-mock.com/orders?type=list')
          .then(function(response) {
            return response.data;
          })
          .then(this.onOrdersLoad.bind(this));

    })
    .controller('OrderDetailController', function(orderData) {
      this.order = orderData;
    });