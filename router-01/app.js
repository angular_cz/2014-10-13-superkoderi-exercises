angular.module('orderAdministration', ['ngRoute'])
    .config(function($routeProvider) {

    })
    .controller('OrderListController', function($http) {
      this.orders = [];

      this.statuses = {
        new : 'Nová',
        made: 'Vyrobená',
        cancelled: 'Zrušená',
        paid: 'Zaplacená',
        sent: 'Odeslaná'
      };

      this.onOrdersLoad = function(orders) {
        this.orders = orders;
      };

      $http.get('http://angularczorders.apiary-mock.com/orders?type=list')
          .then(function(response) {
            return response.data;
          })
          .then(this.onOrdersLoad.bind(this));

    })
    .controller('OrderDetailController', function(orderData) {
      this.order = orderData;
    });