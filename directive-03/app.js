'use strict';

angular.module('directiveApp', [])

  .controller('RatingCtrl', function () {
    this.restaurant = {
      personal: 2,
      food: 3,
      place: 4
    };
  })

  .directive("rating", function () {
    return {
      restrict: "A",
      templateUrl: "rating.html",
      link: function (scope, element, attr) {

        scope.getStarImage = function (starNumber) {
          return starNumber <= scope.rating.restaurant.personal ? "star.png" : "star_bw.png";
        };
      }
    };
  })

  .directive("averageRating", function () {
    return {
      restrict: "A",
      transclude: true,
      templateUrl: "average-rating.html",
    };
  })

  .service("AverageCounter", function () {
    var count = 0;
    var sum = 0;

    this.addRating = function (rating) {
      count++;
      sum += parseInt(rating);
    };

    this.getAverage = function () {
      if (count === 0) {
        return 0;
      }
      
      return Math.round((sum / (count * 5)) * 100);      
    };
  });