Directive-03

1. direktiva rating - ztratila se popiska Obsluha umístěná uvnitř tagu 
  - přidat transclude do definice direktivy
  - přidat ng-transclude do šablony

2. chceme použít direktivu víckrát
  - vytvoříme v definici direktivy scope, zvolíme rating a dvoucestný databinding
  - použijeme scope.rating na místě scope.rating.restaurant.personal
  - v index.html předáme direktivě v atributu rating hodnocení obsluhy(rating.restaurant.personal)
  - použijeme direktivu také pro atributy food a place

3. direktiva averageRating - průměrné hodnocení
  - použijte direktivu average-rating na nadřazený div
  - injektnout do definice této directivy service AvarageCounter
  - přidat direktivě controller
  - do kontrolleru přidat metody addRating(rating), getAverage()
  - metody budou provolávat stejnojmenné metody service AverageCounter
  - controllerAs nastavit na "average"
  - použít metodu average.getAverage() v šabloně direktivy - average-rating.html

4. komunikace mezi direktivami 
  - přidat podmíněnou závislost direktivy rating na averagerating require:"?^averageRating"
  - pridat proměnnou controller jako poslední parametr metody link
  - zavolat na controlleru metodu addRating(rating)