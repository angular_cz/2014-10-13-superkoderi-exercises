Controller-01

1. inicializace $scope 
 - použití controlleru v html
 - inicializace dat v controlleru

user = {
    name: 'Petr',
    surname: 'Novák',
    yearOfBirth: 1982
  };

2. oživte input prvky

3. definujte funkci pro výpočet věku, který je již volána v šabloně

------------

4. upravte příklad na použití controller as

------------

5. doplňte do getAge console.log a sledujte při změně jakých dat je metoda volána
6. použijte pro jméno a getAge one-time binding, sledujte změnu chování