Filter-02

1. všimněte si limitTo
  - je zde 16 000 záznamů měst, proto je výpis rovnou omezen na rozumnou hodnotu
  
2. doplňte hledání
  - pole s ng-model reality.search už tu je, použijte jej ve filteru před limitTo
  - vyzkoušejte v hledacím poli zadání jméno města i psč

3. samostatné hledání podle typu
  - druhé pole, upravte tak, aby hledalo jen podle name
  - třetí pole, upravte tak, aby hledalo jen podle psč
  - původní pole upravte, aby stále hledalo ve všech polích - ($)

4- řazení
  - použijte model radiobuttonů ve filteru orderBy
  - upravte radiobutonny tak aby řadily psč vzestupně a sestupně
