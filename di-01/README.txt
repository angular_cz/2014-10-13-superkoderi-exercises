DI - 01

1. vytvořit value defaultProduct, 
  - přesunout do ní nastavení productu z controlleru.
  - použít tuto value v controlleru

2. vytvoření factory Calculator
  - vytvořit metodu getPrice(product)
  - použít v controlleru a zavolat

3. vytvoření service Logger
  - vytvořit metodu log(message), která loguje do console
  - použít Logger v Calculator a zaměnit volání console za factory