'use strict';

angular.module('diApp', [])

  .controller('CalculatorCtrl', function() {
    this.product = {
      pageSize: 'A5',
      numberOfPages: 50
    };

    this.getPrice = function() {
      var product = this.product;
      
      var baseCoverPrice = 70;
      var pagePrice = 3;
            
      var price = 0;

      switch (product.pageSize) {
        case 'A6': price += baseCoverPrice;
          break;
        case 'A5': price += baseCoverPrice + 20;
          break;
        case 'A4': price += baseCoverPrice + 40;          
          break;
      }

      var pagesPrice = Math.ceil(product.numberOfPages / 5) * pagePrice;
      price += pagesPrice;

      console.log('Price : ' + price + " - " + product.numberOfPages + " stran, " + product.pageSize);
      return price;
    };
  });