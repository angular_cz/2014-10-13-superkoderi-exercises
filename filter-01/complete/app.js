'use strict';

angular.module('filterApp', [])
  .controller('FilterCtrl', function () {
    this.inputText = "Ahoj světe ...";
  })
  
  .filter("reverse", function () {
    return function (input, prefix) {
      input = input || "";
      prefix = prefix || "";

      var reversed = input.split("").reverse().join("");
      return prefix + reversed;
    };
  });