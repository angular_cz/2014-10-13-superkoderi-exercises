Filter-01

1. přidat filter uppercase tam kde má být text velkým

2. vytvořit vlastní filter reverse 
 - vracená funkce by měla mít dva argumenty - input, prefix
 - pozor na undefined u prefixu (prefix = prefix || "")
 - funkce pro otočení - input.split("").reverse().join("");

3. použití filteru
 - použít filter reverse
 - použít filter reverse v kombinaci s uppercase
 - použít filter revers spolu s nějakým hezkým prefixem, třeba "Le "