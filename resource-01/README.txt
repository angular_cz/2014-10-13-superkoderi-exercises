Resource-01

1. do definice modulu přidejte závislost ngResource

2. Ve factory Orders inicializujte $resource pro url v konstantě 
  - jako url použijte : REST_URI + '/orders/:id
  - parametr id namapujte na @id

3. V OrderListControlleru použijte vytvořenou factory a pomocí volání query() naplňte this.orders

4. Získání dat pro detail v configu ve funkci která vyhodnocuje orderData 
  - pro získání využijte factory s odpovídajícím id
  - návratovou hodnotou funkce orderData by mělo být promise, na hodnotu vrácenou .get použijte .$promise

5. Vytvořte nový objekt
  - V metodě createNewOrder vytvořte nový objekt pomocí volání new na factory
  - nastavte do vytvořeného objektu name a email z this.order
  - vytvořený objekt uložit pomocí $save 
  - po uložení přesměrovat na detail pomocí $location.path("/detail/" + id);

6. Vyzkoušejte rozhraní a podívejte se jaké požadavky aplikace odesílá
  - jedná se o fiktivní api tak se neděste že se vytvořený uživatel neobjeví v seznamu
  - podívejte se:
    - na získání seznamu
    - získání detailu
    - vytvoření nového uživatele a vrácená hodnota s id