angular.module('orderAdministration', ['ngRoute'])
  .constant('REST_URI', 'http://angularczresource.apiary-mock.com')
  .config(function ($routeProvider) {

    $routeProvider
      .when('/orders', {
        templateUrl: 'orderList.html',
        controller: 'OrderListController',
        controllerAs: 'list'
      })
      .when('/detail/:id', {
        templateUrl: 'orderDetail.html',
        controller: 'OrderDetailController',
        controllerAs: 'detail',
        resolve: {
          orderData: function (Orders, $route) {
            var id = $route.current.params.id;

            return null;
          }
        }
      })
      .otherwise('/orders');
  })
  .factory('Orders', function (REST_URI, $resource) {
    return null;
  })
  .controller('OrderListController', function (Orders, $location) {
    this.orders = [];

    this.statuses = {
      new : 'Nová',
      made: 'Vyrobená',
      cancelled: 'Zrušená',
      paid: 'Zaplacená',
      sent: 'Odeslaná'
    };
    
    this.order = {
      name : 'New User',
      email : 'new.user@user.cz'
    };

    this.createNewOrder = function () {

    };
  })
  .controller('OrderDetailController', function (orderData) {
    this.order = orderData;
  });