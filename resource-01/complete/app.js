angular.module('orderAdministration', ['ngRoute', 'ngResource'])
  .constant('REST_URI', 'http://angularczresource.apiary-mock.com')
  .config(function ($routeProvider) {

    $routeProvider
      .when('/orders', {
        templateUrl: 'orderList.html',
        controller: 'OrderListController',
        controllerAs: 'list'
      })
      .when('/detail/:id', {
        templateUrl: 'orderDetail.html',
        controller: 'OrderDetailController',
        controllerAs: 'detail',
        resolve: {
          orderData: function (Orders, $route) {
            var id = $route.current.params.id;
            return Orders.get({'id' : id}).$promise;
          }
        }
      })
      .otherwise('/orders');
  })
  .factory('Orders', function (REST_URI, $resource) {
    return $resource(REST_URI + '/orders/:id', {"id": "@id"});
  })
  .controller('OrderListController', function (Orders, $location) {
    this.orders = Orders.query();

    this.statuses = {
      new : 'Nová',
      made: 'Vyrobená',
      cancelled: 'Zrušená',
      paid: 'Zaplacená',
      sent: 'Odeslaná'
    };

    this.order = {
      name : 'New User',
      email : 'new.user@user.cz'
    };
    
    this.createNewOrder = function () {
      var item = new Orders;
      item.name = this.order.name;
      item.email = this.order.email;
      
      item.$save().then(function() {
        $location.path("/detail/" + item.id);
      });
    };
    
  })
  .controller('OrderDetailController', function (orderData) {
    this.order = orderData;
  });