Directive-01

1. Podle výběru typu zobrazte div s odpovídajícími parametry - domu nebo pozemku
  - podle reality.type - ground/house
  - jednou použijte ng-if a podruhé ng-show
  - podívejte se na rozdíl ve firbugu

2. Seznam lokalit 
  - použijte ng-repeat na divu, který chcete opakovat
  - vykreslete jej z pole reality.localities, jako value můžete použít hodnotu $index

3. sekci informace přesuňte do samostatného souboru
  - použijte ng-include jako atribut
  - pozor na expression - '...'

4. vytvoření záložek v sekci informace
  - doplňte  tlačítka o událost pro kliknutí ng-click, využijte volání info.setTab(<nazev>)
  - jako název použijte "about", "reference", "contacts"
  - použijte ng-switch na div nadřazený budoucím záložkám, využijte funkci info.getActiveTab()
  - ng-switch-when pro jednotlivé divy záložek by měl odpovídat názvům použitým výše.
  
5. zvýraznění aktuálně vybraného tabu
  - ng-class ve formátu {}, 
  - jako třídu na levé straně použijte 'btn-info'
  - jako podmínku funkci info.isTabActive(<nazev>)

5. Odstranění bliknutí před tím než se skryjou divy typu

6. Vyzkoušejte batarang a zvýraznění scope
  - Enable
  - Options -> scope
  