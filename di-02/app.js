'use strict';

angular.module('diApp', [])

  .value('defaultProduct', {
    pageSize: 'A5',
    numberOfPages: 50
  })
  
  .factory('Calculator', function (Logger) {
    return {
      getPrice: function (product) {
        var baseCoverPrice = 70;
        var pagePrice = 3;

        var price = 0;

        switch (product.pageSize) {
          case 'A6':
            price += baseCoverPrice;
            break;
          case 'A5':
            price += baseCoverPrice + 20;
            break;
          case 'A4':
            price += baseCoverPrice + 40;
            break;
        }

        var pagesPrice = Math.ceil(product.numberOfPages / 5) * pagePrice;
        price += pagesPrice;

        Logger.log('Price : ' + price + " - " + product.numberOfPages + " stran, " + product.pageSize);
        return price;
      }
    };
  })
  
  .service('Logger', function(){
    this.log = function(message){
      console.log(message);
    };
  })
  
  .controller('CalculatorCtrl', function (defaultProduct, Calculator) {
    this.product = angular.copy(defaultProduct);

    this.getPrice = function () {
      return Calculator.getPrice(this.product);
    };
  });