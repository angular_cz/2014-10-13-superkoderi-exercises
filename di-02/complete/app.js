'use strict';

angular.module('diApp', [])
  .constant('PAGE_PRICE', 4)

  .config(function(CalculatorProvider, PAGE_PRICE){
    CalculatorProvider.setPagePrice(PAGE_PRICE);
  })
  
  .value('defaultProduct', {
    pageSize: 'A5',
    numberOfPages: 50
  })
  
  .provider('Calculator', function () {
    var baseCoverPrice = 70;
    var pagePrice = 3;
    
    this.setBaseCoverPrice = function(price) {
      baseCoverPrice = price;
    };
    
    this.setPagePrice = function(price) {
      pagePrice = price;
    };
          
    this.$get = function (Logger) {
      return {
        getPrice: function (product) {

          var price = 0;

          switch (product.pageSize) {
            case 'A6':
              price += baseCoverPrice;
              break;
            case 'A5':
              price += baseCoverPrice + 20;
              break;
            case 'A4':
              price += baseCoverPrice + 40;
              break;
          }

          var pagesPrice = Math.ceil(product.numberOfPages / 5) * pagePrice;
          price += pagesPrice;

          Logger.log('Price : ' + price + " - " + product.numberOfPages + " stran, " + product.pageSize);
          return price;
        }
      };
    };
  })
  
  .service('Logger', function () {
    this.log = function (message) {
      console.log(message);
    };
  })
  
  .controller('CalculatorCtrl', function (defaultProduct, Calculator) {
    this.product = angular.copy(defaultProduct);

    this.getPrice = function () {
      return Calculator.getPrice(this.product);
    };
  });