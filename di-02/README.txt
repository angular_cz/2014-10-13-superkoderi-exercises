DI-02

1. přepsat factory Calculator na provider
  - obsah factory bude vracen metodou $get
  - přesunout injektnutý Logger na správné místo

2. příprava konfigurace provideru
  - přesunout definici atributů do provideru 
      var baseCoverPrice = 70;
      var pagePrice = 3;
  - přidat settery - setBaseCoverPrice(price), setPagePrice(price)

3. konfigurace provideru
  - přidat metodu .config
  - injektnout CalculatorProvider
  - nastavit zavolat setPagePrice(4)

4. přidat konstantu PAGE_PRICE, obsahující cenu
  - injektnout konstantu do .config a použít v setteru