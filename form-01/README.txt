Form-01

1. vyzkoušejte že formulář funguje
  jak je navázaný na  this.user
  jak se mění třídy formuláře a prvků

2. validace
  přiřaďte formulář do scope (pojmenování)
  zajistěte správné zobrazení hlášek pro
    jméno uživatele - povinná položka
    email - povinná položka + typ email

3. vlastní validátor na formát telefonního čísla
   vytvořte directivu validátoru
   zobrazujte chybovou zprávu dle výsledku validace
   zjistěte jak se validátor projeví v css třídách prvku

4. propagace položky City až po opuštění pole
  ng-model-options (https://docs.angularjs.org/api/ng/directive/ngModelOptions)

5. tlačítka a akce

    tlačítka vyvolájí metody:
      reset -> detail.reset()
      clear storage -> detail.clear()

    zajistěte, aby bylo tláčítko reset disabled, když nejsou ve formuláři neuložené změny

6. ukládání do store
    odeslání formuláře provede detail.save()
    vyplňte formulář, uložte data a sledujte kam se propagují další změny hodnot
