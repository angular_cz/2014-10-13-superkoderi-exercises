angular.module('FormApp', ['ngMessages', 'ngStorage']).
  controller('UserDetailController', function($scope, userStorage) {
    this.userStorage = userStorage;

    this.clear = function() {
      userStorage.clear();
      this.reset();
    };

    this.save = function() {
      if ($scope.userForm.$invalid) {
        return;
      }

      userStorage.save(this.user);
    };

    this.reset = function() {
      this.user = userStorage.get();
    };

    this.reset();
  })
  .directive('validateCzechPhoneNumber', function() {
    var pattern = /^(\+420)?( ?\d{3}){3}$/;

    return {
    };
  })
  .service('userStorage', function($localStorage) {
    this.userStorage = $localStorage.$default({
      user: {
        name: 'unknown name'
      }
    });

    this.get = function() {
      return angular.copy(this.userStorage.user);
    };

    this.save = function(user) {
      this.userStorage.user = user;
    };

    this.clear = function() {
      this.userStorage.$reset({
        user: {}
      });
    };

  });