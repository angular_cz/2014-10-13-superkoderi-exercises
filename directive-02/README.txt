Directive-02;

1. doplňte definiční objekt directivy baconIpsum
  - definujte metodu link a vytvořte pole texts

scope.texts = ['první odstavec']

  - definujte šablonu, vypište texty ve scope.texts 

  - naplňte proměnnou daty získanými ze služby (volání bez parametrů)

2. TODO 2 - directiva s parametrem
  - předejte hodnotu attributu jako parametr generátoru
  - vyřešte problém s "přepisováním dat"

3. přidejte parametr start-with-lorem 
  - pokud je definován attribut, druhý parametr metody generátoru bude true

4. dynamický změna titulku
  - zobrazte hodnotu parametru title jako h3
  - zkontrolujte, že se mění při změně inputu
  - kde všude můžete použít one time binding?