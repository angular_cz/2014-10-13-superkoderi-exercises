angular.module('bacon', ['ipsumService'])
    .controller('baconController', function () {
      this.title4 = 'bacon-ipsum s title';
    })
    .directive('baconIpsum', function (generator) {
      /*
       * {Promise} generator.paras(number = 1, startWithLorem = false)
       */
      return {
        scope:{
          title: '@'
        },
        link:function(scope, element, attributes) {
          generator.paras(attributes.paras, attributes.hasOwnProperty('startWithLorem')).then(function(data) {
            scope.texts = data;
          });
        },
        templateUrl: 'baconIpsum.html'
      };
    });
