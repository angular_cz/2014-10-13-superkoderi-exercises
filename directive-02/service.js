angular.module('ipsumService', [])
    .service('generator', function ($http) {
      this.paras = function (number, startWithLorem) {
        number = number || 1;
        return $http
            .get('http://corsproxy-angularcz.rhcloud.com/baconipsum.com/api/?type=meat-and-filler', {
              params: {
                type: 'meat-and-filler',
                paras: number,
                'start-with-lorem': startWithLorem ? 1 : 0
              }
            })
            .then(
                function (response) {
                  return response.data;
                },
                function () {
                  return ['nepodařilo se získat text'];
                });
      };
    });