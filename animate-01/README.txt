Animate-01

1. ng-if 
  - podívejte se na css .type
  - využívá animate.css a má definovány jen vstupní události 
  - použijte třídu .type na bloky oba ng-if
  - kód najdete v reality.html
  - vyzkoušejte změnu typu pozemku

2. ng-repeat 
  - podívejte se na css .cities
  - použít třídu .cities na element s ng-repeat
  - zkuste hledání lokalit

3. ng-switch jako slider
  - podívejte se na css .information
  - použijte třídu information na elementy ng-switch-when
  - vyzkoušejte jak se chová navigace ve slideru

4. ngForm
  - podívejte se na css .validForm, které definují vzhled pro ng-invalid
  - přidejte třídu .validForm na oba elementy s třídou form-inline, nebo na druhý element v reality.html
  - vyzkoušejte zadání nečíselné hodnoty do rozlohy pozemku

4. ng-view
  - podívejte se na css .page
  - použijte třídu .page na ng-view v index.html
  - vyzkoušejte stisk tlačítka odeslat a poté akce zpět a vpřed v prohlížeči
  

